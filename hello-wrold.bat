#Download and Run MSI package for Automated install
$uri = "http://hicap.frontmotion.com.s3.amazonaws.com/Firefox/Firefox-53.0.3/Firefox-53.0.3-en-US.msi"
$out = "c:\FireFoxInstaller.msi"

Function Download_MSI_FireFox_Installer{
Invoke-WebRequest -uri $uri -OutFile $out
$msifile = Get-ChildItem -Path $out -File -Filter '*.ms*' 
write-host "FireFox MSI $msifile "
}

Function Install_FireFox{
$FileExists = Test-Path $msifile -IsValid
$DataStamp = get-date -Format yyyyMMddTHHmmss
$logFile = '{0}-{1}.log' -f $msifile.fullname,$DataStamp
$MSIArguments = @(
    "/i"
    ('"{0}"' -f $msifile.fullname)
    "/qn"
    "/norestart"
    "/L*v"
    $logFile
)
If ($FileExists -eq $True)
{
Start-Process "msiexec.exe" -ArgumentList $MSIArguments -passthru | wait-process
write-host "Finished msi "$msifile
}

Else {Write-Host "File doesn't exists"}
}
Download_MSI_FireFox_Installer
Install_FireFox
